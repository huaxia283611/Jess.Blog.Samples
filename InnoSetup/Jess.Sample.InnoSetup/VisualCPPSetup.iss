;Visual C++环境检测安装包示例
        
#define appid "{F3234165-547C-4DE1-AF86-767178AF3FAC}"         
#define appversion "1.0.0.0"

[Setup]
AppName=HelloWorld      
AppId={{#appid}         
AppVersion={#appversion}
;AppVersion=1.0.0.0
WizardStyle=modern
DefaultDirName={autopf}\HelloWorld
DefaultGroupName=HelloWorld
UninstallDisplayIcon={app}\HelloWorld.exe
Compression=lzma2
SolidCompression=yes
OutputDir=userdocs:Inno Setup Examples Output
OutputBaseFilename=VisualCPPSetup

[Files]
Source: "HelloWorld.exe"; DestDir: "{app}";Components:main          
Source:".\VC_redist.x86.exe";DestDir:"{tmp}";DestName:"vcredistx86.exe";Flags:ignoreversion

[Languages]
Name:"cn";MessagesFile:"compiler:Languages\ChineseSimplified.isl"
   
[Icons]
Name: "{group}\HelloWorld"; Filename: "{app}\HelloWorld.exe"
Name: "{commondesktop}\HelloWorld"; Filename: "{app}\HelloWorld.exe"
Name: "{group}\卸载HelloWorld"; Filename: "{uninstallexe}"      


[Types]
Name: "full"; Description: "完全安装"
Name: "compact"; Description: "最小安装"
Name:"custom";Description:"自定义安装"; Flags:iscustom

[Components]
Name:main;Description:"主程序";Types:full



[Code]

//vc++ 安装环境检查 plat:x64 x86
function IsVCPPDetected(version:string;plat:string): Boolean;
var 
    success: Boolean;
    key,versionkey:string;
    build,versionBuild:cardinal;
begin
    versionkey:=version;
    versionBuild:=0;

    case version of        
        '13':
            begin
            versionkey:='12.0';
            versionBuild:=40660;
            end;

        '15':
            begin
            versionkey:='14.0';
            versionBuild:=23026;
            end;
        '19':
            begin
            versionkey:='14.0';
            versionBuild:=28720;
            end;

    end;

    key:='SOFTWARE\Wow6432Node\Microsoft\VisualStudio\'+versionkey+'\VC\Runtimes\'+plat;

    success:=RegQueryDWordValue(HKLM, key, 'Bld', build);
    success:=success and (build>=versionBuild);

    result := success;
end;

              
//安装包初始化                      
function InitializeSetup(): Boolean;
var Path:string ; 
    ResultCode: Integer; 
    Success:Boolean;
begin

  Success:=true;

  if not IsVCPPDetected('19','x86') then
  begin
    MsgBox('软件运行需要 Microsoft Visual C++ 2019 Redistributable x86, 即将开始安装vc redist x86。', mbInformation, MB_OK);

    ExtractTemporaryFile('vcredistx86.exe');
    Path:=ExpandConstant('{tmp}\vcredistx86.exe')
    Exec(Path, '', '', SW_SHOWNORMAL, ewWaitUntilTerminated, ResultCode);

    if not IsVCPPDetected('19','x86') then
    begin
      if MsgBox('Microsoft Visual C++ 2019 Redistributable x86组件安装失败，部分功能无法使用，是否继续安装！',mbInformation,MB_YesNo) = IDYES then
      begin
      Success:=true;
       end else begin 
      Success := false;
      result:=Success;
      Exit;
      end;
    end
    else
    begin end;
  end;

  result := Success;         
end;

