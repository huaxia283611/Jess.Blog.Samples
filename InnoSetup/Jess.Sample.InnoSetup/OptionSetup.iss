;安装部件控制安装包示例

[Setup]
AppName=HelloWorld
AppVersion=1.0.0.0
WizardStyle=modern
DefaultDirName={autopf}\HelloWorld
DefaultGroupName=HelloWorld
UninstallDisplayIcon={app}\HelloWorld.exe
Compression=lzma2
SolidCompression=yes
OutputDir=userdocs:Inno Setup Examples Output
OutputBaseFilename=OptionSetup

[Files]
Source: "HelloWorld.exe"; DestDir: "{app}";Components:main

[Languages]
Name:"cn";MessagesFile:"compiler:Languages\ChineseSimplified.isl"
   
[Icons]
Name: "{group}\HelloWorld"; Filename: "{app}\HelloWorld.exe"
Name: "{commondesktop}\HelloWorld"; Filename: "{app}\HelloWorld.exe"
Name: "{group}\卸载HelloWorld"; Filename: "{uninstallexe}"      


[Types]
Name: "full"; Description: "完全安装"
Name: "compact"; Description: "最小安装"
Name:"custom";Description:"自定义安装"; Flags:iscustom

[Components]
Name:main;Description:"主程序";Types:full