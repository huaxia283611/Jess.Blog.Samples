;升级安装包示例
        
#define appid "{F3234165-547C-4DE1-AF86-767178AF3FAC}"         
#define appversion "1.0.0.2"

[Setup]
AppName=HelloWorld      
AppId={{#appid}         
AppVersion={#appversion}
;AppVersion=1.0.0.0
WizardStyle=modern
DefaultDirName={autopf}\HelloWorld
DefaultGroupName=HelloWorld
UninstallDisplayIcon={app}\HelloWorld.exe
Compression=lzma2
SolidCompression=yes
OutputDir=userdocs:Inno Setup Examples Output
OutputBaseFilename=UpdateSetup

[Files]
Source: "HelloWorld.exe"; DestDir: "{app}";Components:main

[Languages]
Name:"cn";MessagesFile:"compiler:Languages\ChineseSimplified.isl"
   
[Icons]
Name: "{group}\HelloWorld"; Filename: "{app}\HelloWorld.exe"
Name: "{commondesktop}\HelloWorld"; Filename: "{app}\HelloWorld.exe"
Name: "{group}\卸载HelloWorld"; Filename: "{uninstallexe}"      


[Types]
Name: "full"; Description: "完全安装"
Name: "compact"; Description: "最小安装"
Name:"custom";Description:"自定义安装"; Flags:iscustom

[Components]
Name:main;Description:"主程序";Types:full



[Code]
                
//-------------------------------------------------------
//旧版本检测，安装新版本
function GetNumber(var temp: String): Integer;
var
  part: String;
  pos1: Integer;
begin
  if Length(temp) = 0 then
  begin
    Result := -1;
    Exit;
  end;
    pos1 := Pos('.', temp);
    if (pos1 = 0) then
    begin
      Result := StrToInt(temp);
    temp := '';
    end
    else
    begin
    part := Copy(temp, 1, pos1 - 1);
      temp := Copy(temp, pos1 + 1, Length(temp));
      Result := StrToInt(part);
    end;
end;
 
function CompareInner(var temp1, temp2: String): Integer;
var
  num1, num2: Integer;
begin
    num1 := GetNumber(temp1);
  num2 := GetNumber(temp2);
  if (num1 = -1) or (num2 = -1) then
  begin
    Result := 0;
    Exit;
  end;
      if (num1 > num2) then
      begin
        Result := 1;
      end
      else if (num1 < num2) then
      begin
        Result := -1;
      end
      else
      begin
        Result := CompareInner(temp1, temp2);
      end;
end;
 
function CompareVersion(str1, str2: String): Integer;
var
  temp1, temp2: String;
begin
    temp1 := str1;
    temp2 := str2;
    Result := CompareInner(temp1, temp2);
end;

function UpdateSetup(): Boolean;
var
  oldVersion: String;
  uninstaller: String;
  ErrorCode: Integer;
begin

  if RegKeyExists(HKEY_LOCAL_MACHINE,'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{#appid}_is1') then
  begin
    RegQueryStringValue(HKEY_LOCAL_MACHINE,'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{#appid}_is1','DisplayVersion', oldVersion);
    if (CompareVersion(oldVersion, '{#appversion}') <= 0) then
    begin
      //if MsgBox('已安装版本：' + oldVersion + ' 的应用. 是否继续卸载旧版本，并安装新版本?',mbConfirmation, MB_YESNO) = IDYES then
      //begin
      //    RegQueryStringValue(HKEY_LOCAL_MACHINE,'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{#appid}_is1','UninstallString', uninstaller);
      //    ShellExec('runas', uninstaller, '/SILENT', '', SW_HIDE, ewWaitUntilTerminated, ErrorCode);
      //    Result := True;
      //end     
       if MsgBox('已安装版本：' + oldVersion + ' 的应用. 是否覆盖安装新版本?',mbConfirmation, MB_YESNO) = IDYES then
      begin          
          Result := True;
      end     
      else
      begin
        Result := False;
      end;
    end
    else
    begin
      MsgBox('已安装较新版本（ ' + oldVersion + ' ）的应用。安装程序即将退出',mbInformation, MB_OK);
      Result := False;
    end;
  end
  else
  begin
    Result := True;
  end;
end;
//-----------------------------------------------------


//-----------------------------------------
//关闭应用
procedure CloseApp(AppName: String);
var
  WbemLocator : Variant;
  WMIService   : Variant;
  WbemObjectSet: Variant;
  WbemObject   : Variant;
begin;
  WbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
  WMIService := WbemLocator.ConnectServer('localhost', 'root\CIMV2');
  WbemObjectSet := WMIService.ExecQuery('SELECT * FROM Win32_Process Where Name="' + AppName + '"');
  if not VarIsNull(WbemObjectSet) and (WbemObjectSet.Count > 0) then
  begin
    WbemObject := WbemObjectSet.ItemIndex(0);
    if not VarIsNull(WbemObject) then
    begin
      WbemObject.Terminate();
      WbemObject := Unassigned;
    end;
  end;
end;
//--------------------------------------
              
//安装包初始化                      
function InitializeSetup(): Boolean;
var 
    Success:Boolean;
    IsRunning: Integer;
begin

  Success:=true;

  IsRunning:=FindWindowByWindowName('HelloWorld');
  if IsRunning<>0 then 
  begin            
      if Msgbox('安装程序检测到客户端正在运行。' #13#13 '您必须先关闭它然后单击“是”继续安装，或按“否”退出！', mbConfirmation, MB_YESNO) = idNO then           
      begin
        Success:=false;
      end
      else 
      begin
        CloseApp('HelloWorld.exe');       
      end;
  end else begin end;

  if Success then 
  begin
  //升级检测
      if not UpdateSetup() then
      begin
          Success:=false;
      end
      else begin end;      
  end;

  result := Success;         
end;

