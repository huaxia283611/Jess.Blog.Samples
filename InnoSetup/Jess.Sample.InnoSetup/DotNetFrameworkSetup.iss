;.Net Framework环境检测安装包示例
        
#define appid "{F3234165-547C-4DE1-AF86-767178AF3FAC}"         
#define appversion "1.0.0.0"

[Setup]
AppName=HelloWorld      
AppId={{#appid}         
AppVersion={#appversion}
;AppVersion=1.0.0.0
WizardStyle=modern
DefaultDirName={autopf}\HelloWorld
DefaultGroupName=HelloWorld
UninstallDisplayIcon={app}\HelloWorld.exe
Compression=lzma2
SolidCompression=yes
OutputDir=userdocs:Inno Setup Examples Output
OutputBaseFilename=DotNetFrameworkSetup

[Files]
Source: "HelloWorld.exe"; DestDir: "{app}";Components:main       
Source:".\NDP472-KB4054531-Web.exe";DestDir:"{tmp}";DestName:"dotnet472.exe";Flags:ignoreversion

[Languages]
Name:"cn";MessagesFile:"compiler:Languages\ChineseSimplified.isl"
   
[Icons]
Name: "{group}\HelloWorld"; Filename: "{app}\HelloWorld.exe"
Name: "{commondesktop}\HelloWorld"; Filename: "{app}\HelloWorld.exe"
Name: "{group}\卸载HelloWorld"; Filename: "{uninstallexe}"      


[Types]
Name: "full"; Description: "完全安装"
Name: "compact"; Description: "最小安装"
Name:"custom";Description:"自定义安装"; Flags:iscustom

[Components]
Name:main;Description:"主程序";Types:full



[Code]
//.net framework安装检查  —— 判断指定的.NET Framework版本及service pack是否已经安装
function IsDotNetDetected(version: string; service: cardinal): boolean;
// version --  -- 指定待判断的.NET Framework版本:
//    'v1.1'          .NET Framework 1.1
//    'v2.0'          .NET Framework 2.0
//    'v3.0'          .NET Framework 3.0
//    'v3.5'          .NET Framework 3.5
//    'v4\Client'     .NET Framework 4.0 Client Profile
//    'v4\Full'       .NET Framework 4.0 Full Installation
//    'v4.5'          .NET Framework 4.5
//    'v4.5.1'        .NET Framework 4.5.1
//    'v4.5.2'        .NET Framework 4.5.2
//    'v4.6'          .NET Framework 4.6
//    'v4.6.1'        .NET Framework 4.6.1
//    'v4.6.2'        .NET Framework 4.6.2
//    'v4.7'          .NET Framework 4.7
//    'v4.7.1'        .NET Framework 4.7.1
//    'v4.7.2'        .NET Framework 4.7.2          
//    `v4.8`          .NET Framework 4.8
//
// service -- 指定待判断的service pack版本:
//    0               No service packs required
//    1, 2, etc.      Service pack 1, 2, etc. required
var
    key, versionKey: string;
    install, release, serviceCount, versionRelease: cardinal;
    success: boolean;
begin
    versionKey := version;
    versionRelease := 0;

    // .NET 1.1 and 2.0 embed release number in version key
    if version = 'v1.1' then begin
        versionKey := 'v1.1.4322';
    end else if version = 'v2.0' then begin
        versionKey := 'v2.0.50727';
    end

    // .NET 4.5 and newer install as update to .NET 4.0 Full
    else if Pos('v4.', version) = 1 then begin
        versionKey := 'v4\Full';
        case version of
          'v4.5':   versionRelease := 378389;
          'v4.5.1': versionRelease := 378675; // 378758 on Windows 8 and older
          'v4.5.2': versionRelease := 379893;
          'v4.6':   versionRelease := 393295; // 393297 on Windows 8.1 and older
          'v4.6.1': versionRelease := 394254; // 394271 before Win10 November Update
          'v4.6.2': versionRelease := 394802; // 394806 before Win10 Anniversary Update
          'v4.7':   versionRelease := 460798; // 460805 before Win10 Creators Update
          'v4.7.1': versionRelease := 461308; // 461310 before Win10 Fall Creators Update
          'v4.7.2': versionRelease := 461808; // 461814 before Win10 April 2018 Update
          'v4.8': versionRelease := 528040;
        end;
    end;

    // installation key group for all .NET versions
    key := 'SOFTWARE\Microsoft\NET Framework Setup\NDP\' + versionKey;

    // .NET 3.0 uses value InstallSuccess in subkey Setup
    if Pos('v3.0', version) = 1 then begin
        success := RegQueryDWordValue(HKLM, key + '\Setup', 'InstallSuccess', install);
    end else begin
        success := RegQueryDWordValue(HKLM, key, 'Install', install);
    end;

    // .NET 4.0 and newer use value Servicing instead of SP
    if Pos('v4', version) = 1 then begin
        success := success and RegQueryDWordValue(HKLM, key, 'Servicing', serviceCount);
    end else begin
        success := success and RegQueryDWordValue(HKLM, key, 'SP', serviceCount);
    end;

    // .NET 4.5 and newer use additional value Release
    if versionRelease > 0 then begin
        success := success and RegQueryDWordValue(HKLM, key, 'Release', release);
        success := success and (release >= versionRelease);
    end;

    result := success and (install = 1) and (serviceCount >= service);
end;

              
//安装包初始化                      
function InitializeSetup(): Boolean;
var Path:string ; 
    ResultCode: Integer; 
    Success:Boolean;
begin

  Success:=true;

   if not IsDotNetDetected('v4.7.2', 0) then 
    begin
        MsgBox('软件运行需要 Microsoft .NET Framework 4.7.2, 即将开始安装.NET Framework。', mbInformation, MB_OK);

        ExtractTemporaryFile('dotnet472.exe');
        Path:=ExpandConstant('{tmp}\dotnet472.exe')
        Exec(Path, '', '', SW_SHOWNORMAL, ewWaitUntilTerminated, ResultCode);

        if not IsDotNetDetected('v4.7.2', 0) then
        begin
          if  MsgBox('.Net Framework4.7.2组件安装失败，无法运行程序，是否继续安装！',mbInformation,MB_YESNO) = IDYES then
          begin
          Success:=true
          end else begin
            Success := false;
            result:=Success;
            Exit;
            end;
        end
        else
            Success := true;
    end
    else
        Success := true;

  result := Success;         
end;

