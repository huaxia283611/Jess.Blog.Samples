;

[Setup]
AppName=HelloWorld
AppVersion=1.0.0.0
WizardStyle=modern
DefaultDirName={autopf}\HelloWorld
DefaultGroupName=HelloWorld
UninstallDisplayIcon={app}\HelloWorld.exe
Compression=lzma2
SolidCompression=yes
OutputDir=userdocs:Inno Setup Examples Output
OutputBaseFilename=HelloWorldSetup

[Files]
Source: "HelloWorld.exe"; DestDir: "{app}"