﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Log4netSample
{
    class Program
    {
        static ILog log = LogManager.GetLogger("yourlognameorfullclassname");

        static void Main(string[] args)
        {
            log.Debug("testdebug");
            log.Info("testinfo");
            log.Warn("testwarn");
            log.Error("testerror");
            log.Fatal("testfatal");

            Console.ReadKey();
        }
    }
}
