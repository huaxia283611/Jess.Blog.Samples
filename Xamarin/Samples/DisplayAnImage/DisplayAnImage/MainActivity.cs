﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace DisplayAnImage
{
    [Activity(Label = "DisplayAnImage", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
             SetContentView (Resource.Layout.Main);

            Button button = FindViewById<Button>(Resource.Id.myButton);
            button.Click += delegate
            {
                var imageView = FindViewById<ImageView>(Resource.Id.demoImageView);
                imageView.SetImageResource(Resource.Drawable.sample2);
            };
        }
    }
}

