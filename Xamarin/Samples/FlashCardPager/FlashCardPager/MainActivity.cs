﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V4.View;
using Android.Support.V4.App;

namespace FlashCardPagerWithAppCompat
{
    [Activity(Label = "FlashCardPager", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : FragmentActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            ViewPager viewPager = FindViewById<ViewPager>(Resource.Id.viewpager);
            FlashCardDeck flashCards = new FlashCardDeck();

            FlashCardDeckAdapter adapter = new FlashCardDeckAdapter(SupportFragmentManager, flashCards);
            viewPager.Adapter = adapter;
        }
    }
}

