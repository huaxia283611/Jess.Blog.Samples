﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V4.View;
using Android.Support.V4.App;
using Android.Support.V7.App;

namespace FlashCardPagerWithAppCompat
{
    [Activity(Label = "FlashCardPagerWithAppCompat", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : AppCompatActivity 
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            ViewPager viewPager = FindViewById<ViewPager>(Resource.Id.viewpager);
            FlashCardDeck flashCards = new FlashCardDeck();

            FlashCardDeckAdapter adapter = new FlashCardDeckAdapter(SupportFragmentManager, flashCards);
            viewPager.Adapter = adapter;
        }
    }
}

