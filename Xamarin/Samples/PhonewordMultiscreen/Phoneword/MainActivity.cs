﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Content;
using System.Collections.Generic;

namespace Phoneword
{
    [Activity(Label = "Phone Word", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        static readonly List<string> phoneNumbers = new List<string>();

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            //获取layout中的控件
            EditText phoneNumberText = FindViewById<EditText>(Resource.Id.PhoneNumberText);
            Button translateButton = FindViewById<Button>(Resource.Id.TranslateButton);
            Button callButton = FindViewById<Button>(Resource.Id.CallButton);
            Button callHistoryButton = FindViewById<Button>(Resource.Id.CallHistoryButton);

            //设置拨打按钮不可用
            callButton.Enabled = false;

            // 存储转换后的号码变量
            string translatedNumber = string.Empty;

            translateButton.Click += (object sender, EventArgs e) =>
            {
                // 将字母数字组合的电话号码转化为纯数字号码
                translatedNumber = Core.PhonewordTranslator.ToNumber(phoneNumberText.Text);
                if (String.IsNullOrWhiteSpace(translatedNumber))
                {
                    callButton.Text = "拨打";
                    callButton.Enabled = false;
                }
                else
                {
                    callButton.Text = "拨打 " + translatedNumber;
                    callButton.Enabled = true;
                }
            };

            callButton.Click += (object sender, EventArgs e) =>
            {
                // 点击“拨打”按钮，尝试拨号
                var callDialog = new AlertDialog.Builder(this);
                callDialog.SetMessage("拨打 " + translatedNumber + "?");
                callDialog.SetNeutralButton("拨打", delegate
                {
                    //添加已拨打号码
                    phoneNumbers.Add(translatedNumber);
                    //启用查看拨打历史按钮
                    callHistoryButton.Enabled = true;

                    // 创建拨号器调用对象？——Create intent to dial phone
                    var callIntent = new Intent(Intent.ActionCall);
                    callIntent.SetData(Android.Net.Uri.Parse("tel:" + translatedNumber));
                    StartActivity(callIntent);
                });
                callDialog.SetNegativeButton("取消", delegate { });

                // 显示用户确认对话框
                callDialog.Show();
            };

            //查看拨打历史
            callHistoryButton.Click += (sender, e) =>
              {
                  var intent = new Intent(this, typeof(CallHistoryActivity));
                  intent.PutStringArrayListExtra("phone_numbers",phoneNumbers);
                  StartActivity(intent);
              };
        }
    }
}

