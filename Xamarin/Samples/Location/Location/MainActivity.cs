﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Locations;
using Android.Runtime;
using System;

namespace Location
{
    [Activity(Label = "Location", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity, ILocationListener
    {
        LocationManager locMgr;
        string tag = "MainActivity";
        /// <summary>
        /// 经度
        /// </summary>
        TextView longitude;
        /// <summary>
        /// 纬度
        /// </summary>
        TextView latitude;
        TextView provider;
        Button gps;
        Button network;
        Button passive;
        Button best;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            gps = FindViewById<Button>(Resource.Id.btn_gps);
            network = FindViewById<Button>(Resource.Id.btn_network);
            passive = FindViewById<Button>(Resource.Id.btn_passive);
            best = FindViewById<Button>(Resource.Id.btn_best);

            longitude = FindViewById<TextView>(Resource.Id.tv_longitude);
            latitude = FindViewById<TextView>(Resource.Id.tv_latitude);
            provider = FindViewById<TextView>(Resource.Id.tv_Provider);

        }

        protected override void OnResume()
        {
            base.OnResume();

            locMgr = GetSystemService(Android.Content.Context.LocationService) as LocationManager;

            gps.Click += delegate
            {
                string Provider = LocationManager.GpsProvider;
                if (locMgr.IsProviderEnabled(Provider))
                {
                    locMgr.RemoveUpdates(this);
                    OnLocationChanged(locMgr.GetLastKnownLocation(Provider));
                    locMgr.RequestLocationUpdates(Provider, 500, 1, this);
                }
                else
                {
                    Android.Util.Log.Info(tag, Provider + "不可用，请确认设备的定位服务是否启用？");
                }
            };

            network.Click += delegate
            {
                string Provider = LocationManager.NetworkProvider;
                if (locMgr.IsProviderEnabled(Provider))
                {
                    locMgr.RemoveUpdates(this);
                    OnLocationChanged(locMgr.GetLastKnownLocation(Provider));
                    locMgr.RequestLocationUpdates(Provider, 500, 1, this);
                }
                else
                {
                    Android.Util.Log.Info(tag, Provider + "不可用，请确认设备的定位服务是否启用？");
                }
            };

            passive.Click += delegate
            {
                string Provider = LocationManager.PassiveProvider;
                if (locMgr.IsProviderEnabled(Provider))
                {
                    locMgr.RemoveUpdates(this);
                    OnLocationChanged(locMgr.GetLastKnownLocation(Provider));
                    locMgr.RequestLocationUpdates(Provider, 500, 1, this);
                }
                else
                {
                    Android.Util.Log.Info(tag, Provider + "不可用，请确认设备的定位服务是否启用？");
                }
            };

            best.Click += delegate
            {
                Criteria locationCriteria = new Criteria();

                locationCriteria.Accuracy = Accuracy.Coarse;
                locationCriteria.PowerRequirement = Power.Medium;

                string locationProvider = locMgr.GetBestProvider(locationCriteria, true);

                if (locationProvider != null)
                {
                    locMgr.RemoveUpdates(this);
                    OnLocationChanged(locMgr.GetLastKnownLocation(locationProvider));
                    locMgr.RequestLocationUpdates(locationProvider, 500, 1, this);
                }
                else
                {
                    Android.Util.Log.Info(tag, "没有可用的定位提供程序");
                }
            };

        }

        protected override void OnPause()
        {
            base.OnPause();
            locMgr.RemoveUpdates(this);
        }


        public void OnLocationChanged(Android.Locations.Location location)
        {
            if (location == null)
            {
                return;
            }
            //throw new NotImplementedException();
            latitude.Text = location.Latitude.ToString();
            longitude.Text = location.Longitude.ToString();
            provider.Text = location.Provider.ToString();
        }

        public void OnProviderDisabled(string provider)
        {
            //throw new NotImplementedException();
        }

        public void OnProviderEnabled(string provider)
        {
            //throw new NotImplementedException();
        }

        public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
        {
            //throw new NotImplementedException();
        }
    }
}

