﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace FragmentSample
{
    [Activity(Label = "FragmentSample", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Android.Support.V4.App.FragmentActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.activity_main);
        }
    }
}

