using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Animation;

namespace AnimationSample.Activities
{
    [Activity(Label = "PropertyAnimationActivity")]
    public class PropertyAnimationActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.PropertyAnimationLayout);

            ImageView imgforvalueanimator = FindViewById<ImageView>(Resource.Id.imageViewForValueAnimator);
            TextView tvforobjectanimator = FindViewById<TextView>(Resource.Id.textViewForObjectAnimator);

            ValueAnimator animator = ValueAnimator.OfInt(0, 100);
            animator.SetDuration(1000);
            animator.Start();

            animator.Update += (object sender, ValueAnimator.AnimatorUpdateEventArgs e) => {
                int newvalue = (int)e.Animation.AnimatedValue;
                imgforvalueanimator.Left = newvalue;
            };

            ObjectAnimator objanimator = ObjectAnimator.OfInt(tvforobjectanimator, "Left", 0,100);
            objanimator.SetDuration(1000);
            objanimator.Start();
        }
    }
}