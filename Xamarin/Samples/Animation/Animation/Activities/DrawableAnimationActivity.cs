using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;

namespace AnimationSample.Activities
{
    [Activity(Label = "DrawableAnimationActivity")]
    public class DrawableAnimationActivity : Activity
    {
        AnimationDrawable drawableanimation;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.DrawableAnimationLayout);

            drawableanimation = (Android.Graphics.Drawables.AnimationDrawable)Resources.GetDrawable(Resource.Drawable.drawableanimation);

            ImageView img = FindViewById<ImageView>(Resource.Id.imageView1);
            img.SetImageDrawable(drawableanimation);

            drawableanimation.Start();

        }
    }
}