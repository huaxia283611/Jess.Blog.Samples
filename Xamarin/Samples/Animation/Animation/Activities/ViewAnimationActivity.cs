using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Views.Animations;

namespace AnimationSample.Activities
{
    [Activity(Label = "ViewAnimationActivity")]
    public class ViewAnimationActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.ViewAnimationLayout);

            Animation myAnimation = AnimationUtils.LoadAnimation(this,Resource.Animation.myviewanimation);
            ImageView myImage = FindViewById<ImageView>(Resource.Id.imageView1);
            myImage.StartAnimation(myAnimation);
        }
    }
}