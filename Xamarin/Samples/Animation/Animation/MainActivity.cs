﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using AnimationSample.Activities;

namespace AnimationSample
{
    [Activity(Label = "AnimationSample", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            Button btn_ViewAnimation = FindViewById<Button>(Resource.Id.btn_ViewAnimation);
            Button btn_PropertyAnimation = FindViewById<Button>(Resource.Id.btn_PropertyAnimation);
            Button btn_DrawableAnimation = FindViewById<Button>(Resource.Id.btn_DrawableAnimation);

            btn_ViewAnimation.Click += delegate {
                Intent i = new Intent(this,typeof(ViewAnimationActivity));
                this.StartActivity(i);
            };
            btn_PropertyAnimation.Click += delegate
            {
                Intent i = new Intent(this, typeof(PropertyAnimationActivity));
                this.StartActivity(i);
            };
            btn_DrawableAnimation.Click += delegate
            {
                Intent i = new Intent(this, typeof(DrawableAnimationActivity));
                this.StartActivity(i);
            };
        }
    }
}

