
# 官方已经推出中文文档

## 官方文档 

* [项目地址](https://github.com/MicrosoftDocs/xamarin-docs.zh-cn)
* [文档部署地址](https://docs.microsoft.com/zh-cn/xamarin/)

> 目前官方文档大部分内容应该是机器翻译，大概理解还是可以的。    
> 另外，感兴趣的同学，可以加入官方文档的翻译工作。不过需要注意的是，翻译有一些风格要求或术语要求等，可以参考微软给出的[本地风格化指南](https://www.microsoft.com/zh-cn/language/StyleGuides)，以及提供的[术语搜索翻译](https://www.microsoft.com/zh-cn/Language/)功能


## 关于微软文档

目前微软提供专门的文档/api构建工具：[docfx](https://github.com/dotnet/docfx) —— 感兴趣的可以去研究下，强大到突破天际，但我还不会用。


# XamarinDoc

## 项目介绍

项目为官方文档翻译项目，目的是个人学习——由于国内大部分介绍都仅限于Hello，World，故做此整理

## 项目结构

* Guides为官方指南翻译部分
* Samples为官方文档中的示例——自己按照文档练习所建立
* PersionalExperience为个人经验——遇到的问题解决方案整理

## 文档索引页

* [索引页](./index.md)
    * [开发指南索引](Guides/index.md)

[index]: http://www.shisujie.com/blog/XamarinIndex
* [Xamarin博客索引目录][index]  


## 本项目的后续发展

后期将作为个人学习整理Xamarin相关经验总结，官方文档翻译部分将会停止更新。