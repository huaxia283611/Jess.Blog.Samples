﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamlSamples
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();


            Button button = new Button
            {
                Text = "Navigate!",
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };

            button.Clicked += async (sender, args) =>
            {
                //await Navigation.PushAsync(new HelloXamlPage());
                //await Navigation.PushAsync(new XamlPlusCodePage());
                //await Navigation.PushAsync(new GridDemoPage());
                //await Navigation.PushAsync(new AbsoluteDemoPage());
                //await Navigation.PushAsync(new SharedResourcesPage());
                //await Navigation.PushAsync(new StaticConstantsPage());
                //await Navigation.PushAsync(new RelativeLayoutPage());
                //await Navigation.PushAsync(new SliderBindingsPage());
                //await Navigation.PushAsync(new SliderTransformsPage());
                //await Navigation.PushAsync(new ListViewDemoPage());
                //await Navigation.PushAsync(new OneShotDateTimePage());
                //await Navigation.PushAsync(new ClockPage());
                //await Navigation.PushAsync(new HslColorScrollPage());
                await Navigation.PushAsync(new KeypadPage());
                
            };

            //Content = button;

        }


        private async void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            (sender as ListView).SelectedItem = null;

            if (args.SelectedItem != null)
            {
                PageDataViewModel pageData = args.SelectedItem as PageDataViewModel;
                Page page = (Page)Activator.CreateInstance(pageData.Type);
                await Navigation.PushAsync(page);
            }
        }
    }
}
